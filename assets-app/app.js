var app = angular.module('cppApp', ['720kb.datepicker']);

app.service('anchorSmoothScroll', function () {

    this.scrollTo = function (eID) {

        // This scrolling function
        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY);
            return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for (var i = startY; i < stopY; i += step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY += step;
                if (leapY > stopY) leapY = stopY;
                timer++;
            }
            return;
        }
        for (var i = startY; i > stopY; i -= step) {
            setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
            leapY -= step;
            if (leapY < stopY) leapY = stopY;
            timer++;
        }

        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }

        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            }
            return y;
        }

    };

});

app.controller('mainController', function ($scope, $http, $timeout, $location, anchorSmoothScroll) {

    $scope.response = null;
    $scope.questions = [];
    $scope.currentQuestions = [];
    $scope.loading = true;
    $scope.submitting = true;
    $scope.canSubmit = false;
    $scope.submitted = true;
    $scope.userView = true;
    $scope.questionsView = false;
    $scope.userProgress = "loaded";
    $scope.processing = false;


    //pagination
    $scope.defaultpage = 1;
    $scope.answeredTotal = 0;
    $scope.main = {
        page: 1
    };

    $scope.user = {
        name: "",
        gender: "Male",
        emailAddress: "",
        contactNumber: "",
        company: "",
        jobTitle: "",
        nameOfAssesementRequest: "",
        nativeLanguage: "",
        dateOfBirth: ""
    };

    $scope.calculatePages = function () {

        var total = ($scope.questions).length;
        var totalpages = parseInt(total / 10);
        if (total % 10 > 0) {
            totalpages = totalpages + 1;
        }
        var range = [];
        for (var i = 1; i <= totalpages; i++) {
            range.push(i);
        }

        $scope.main.fromviewrange = (($scope.main.page - 1) * 10) + 1;

        if (range[range.length - 1] == $scope.main.page) {
            $scope.main.toviewrange = total;
        } else {
            $scope.main.toviewrange = (($scope.main.page) * 10);
        }
        $scope.main.totalpages = range;
        $scope.main.totalquestions = total;
        $scope.main.pages = totalpages;
    };

    $scope.changeToQView = function () {
        $scope.userView = false;
        $scope.questionsView = true;
        $scope.gotoTopElement();
    };

    $scope.changeToUView = function () {
        $scope.userView = true;
        $scope.questionsView = false;
        $scope.gotoElementU();
    };

    $scope.validateFields = function () {
        var userinfo = $scope.user;
        var correct = 0;
        angular.forEach(userinfo, function (value, key) {
            if (value.length !== 0 && value !== "") {
                correct++;
            }
        });

        if (Object.keys(userinfo).length === correct) {
            $scope.userProgress = 'success';
        } else {
            $scope.userProgress = 'error';
        }

        if ($scope.userProgress == 'success') {
            $scope.changeToQView();
        }
    };

    $scope.changePage = function (newpage) {
        $scope.main.page = newpage;
        $scope.getQuestions();
        $scope.gotoElement();
    };

    $scope.nextPage = function () {
        if ($scope.main.page < $scope.main.pages) {
            $scope.main.page++;
            $scope.getQuestions();
            $scope.gotoElement();
        }

        if ($scope.main.page === $scope.main.pages) {
            $http({
                method: "get",
                url: "http://129.232.233.82:8080/PsychometricApi/api/SheetWriter/ServerWakeup",
                headers: { 'Content-Type': 'application/json' }
            });
        }
    };

    $scope.previousPage = function () {
        $scope.selectedids = [];
        if ($scope.main.page > 1) {
            $scope.main.page--;
            $scope.getQuestions();
            $scope.gotoElement();
        }
    };

    $scope.gotoElement = function () {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        //$location.hash('bottom');

        // call $anchorScroll()
        anchorSmoothScroll.scrollTo("questionBox");

    };
    $scope.gotoTopElement = function () {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        //$location.hash('bottom');

        // call $anchorScroll()
        anchorSmoothScroll.scrollTo("top");

    };
    $scope.gotoElementU = function () {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        //$location.hash('bottom');

        // call $anchorScroll()
        anchorSmoothScroll.scrollTo("usersBox");

    };
    $scope.gotoElementRes = function () {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        //$location.hash('bottom');

        // call $anchorScroll()
        anchorSmoothScroll.scrollTo("resposnes");

    };

    $scope.checkIfAllMarked = function () {

        var questions = $scope.questions;


        angular.forEach(questions, function (data, index) {
            if (data.real === null) {
                $scope.questions[index].unmarked = false;
            } else {
                $scope.questions[index].unmarked = true;
            }

            if ($scope.questions[index].unmarked === true) {
                $scope.canSubmit = true;
            } else {
                $scope.canSubmit = false;
            }

        });

    };

    $scope.checkIfCanSubmit = function () {
        if ($scope.answeredTotal === $scope.questions.length) {
            $scope.canSubmit = true;
        } else {
            $scope.canSubmit = false;
        }

    };

    $scope.markAnswered = function (id) {
        if ($scope.questions[$scope.main.fromviewrange + id - 1].unmarked === true || $scope.questions[$scope.main.fromviewrange + id - 1].unmarked === null) {
            $scope.answeredTotal++;
            $scope.questions[$scope.main.fromviewrange + id - 1].unmarked = false;
        }
        $scope.checkIfCanSubmit();
    };

    $scope.submitForm = function () {
        $scope.checkIfAllMarked();
        if ($scope.canSubmit === true) {
            $scope.showProc(true);
            $timeout(function () {
                var data = {
                    questions: $scope.questions,
                    user: $scope.user
                };
                angular.forEach(data.questions, function (que, index) {
                    if (que.real == "true") {
                        data.questions[index].real = true;
                    } else {
                        data.questions[index].real = false;
                    }
                });

                $http({
                    method: "post",
                    // url: "http://129.232.233.82:8080/PsychometricApi/api/SheetWriter/ProcessReport",
                    url: "http://localhost:2723/api/PsychometricAnalysis/SubmitPsychometricAnalysis",
                    data: JSON.stringify(data),
                    headers: { 'Content-Type': 'application/json' }
                }).success(function (response) {
                    $scope.response = response;
                    if ($scope.response.success) {
                        $scope.submitted = false;
                    }
                    $scope.showProc(false);

                }).error(function (response) {
                    $scope.response = response;
                    $scope.gotoElementRes();
                    $scope.showProc(false);
                });

            }, 1000);
        }
    };

    $scope.getQuestions = function () {
        $scope.calculatePages();
        $scope.currentQuestions = $scope.questions.slice($scope.main.fromviewrange - 1, $scope.main.toviewrange);
    };

    $scope.showProc = function (value) {
        $scope.processing = value;
        // $scope.processing = !$scope.processing;
    };

    $http.get('assets-app/data.json').
        success(function (res) {
            $scope.questions = res.data;

            angular.forEach($scope.questions, function (data, index) {
                $scope.questions[index].id = index + 1;
                $scope.questions[index].unmarked = null;
            });
            if (typeof $scope.questions !== 'undefined' && $scope.questions.length > 0) {
                $scope.loading = false;
            }

            console.log($scope.questions);
            $scope.calculatePages();
            $scope.getQuestions();
        }).
        error(function (data) {
            console.log(data);
        });
});